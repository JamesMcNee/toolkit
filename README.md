# Toolbox

Toolbox is set of useful tools contained within a JAR. These tools provide easy yet powerful functions to aid in software development.

## Installation

Download the latest version of the toolbox, you can then run the jar from the terminal like this: 

`java -jar ~/{PATH_TO_JAR}/toolbox-{VERSION}.jar`

or better yet, create a little script for this (use by typing `toolbox` into your shell:
```
// Add this to ~/.bashrc (or your preferred RC file e.g. ~/.zshrc)

toolbox() {
    java -jar java -jar ~/{PATH_TO_JAR}/toolbox-{VERSION}.jar
}
```

## Tools
\*Will use clipboard value if none specified. <br />
\**Supports `--c` flag to copy result to clipboard.

#### Date Functions
`date epoch`\*\*: Print date as epoch (specify date or now).<br />
`date parse`\*\*: Parse epoch timestamp and display human readable date \*

#### Encode Functions
`encode, encode base64`\*\*: Encode a base64 string. \* <br />
`decode, decode base64`\*\*: Decode a base64 string. \*

#### JSON Functions
`json prettify`\*\*: Take a JSON string and prettify it. \*

#### String Formatting Functions
`string-format csv`\*\*: Take input list and transform it into a CSV. \* <br />
`string-format listify`\*\*: Take input CSV and transform it into a list. \* <br />
`string-format strip-spaces`\*\*: Take input string and strip all spaces. \*

#### HTTP Testing/Timing Functions
`http time`: Take input URL and return performance timing stats. \*