package uk.co.jamesmcnee.toolbox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.util.StringUtils;

@SpringBootApplication
@PropertySources({
		@PropertySource("classpath:application.yml"),
		@PropertySource("classpath:build.properties")
})
public class ToolboxApplication {

	public static void main(String[] args) {
		String[] disabledCommands = {"--spring.shell.command.script.enabled=false", "--spring.shell.command.stacktrace.enabled=false"};
		String[] fullArgs = StringUtils.concatenateStringArrays(args, disabledCommands);

		SpringApplication.run(ToolboxApplication.class, fullArgs);
	}

}
