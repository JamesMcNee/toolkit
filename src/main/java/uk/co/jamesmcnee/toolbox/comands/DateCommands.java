package uk.co.jamesmcnee.toolbox.comands;

import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.service.ClipboardService;

import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static java.util.Objects.nonNull;
import static uk.co.jamesmcnee.toolbox.configuration.ClipboardFallbackParameterResolver.CLIPBOARD_FALLBACK;

@ShellComponent
public class DateCommands {

    private final ClipboardService clipboardService;

    @Autowired
    public DateCommands(ClipboardService clipboardService) {
        this.clipboardService = clipboardService;
    }

    @ShellMethod(value = "Print date as epoch", key = {"date epoch"})
    public List<AttributedString> epoch(@ShellOption(value = {"--date-string"}, help = "Date string to covert to epoch", defaultValue = "") String dateString,
                                        @ShellOption(value = {"-c", "--copy"}, help = "Copy the result list to clipboard", defaultValue = "false") boolean copyToClipboard,
                                        @ShellOption(value = {"--precision"}, help = "Resulting epoch precision milli/second", defaultValue = "millisecond") EpochPrecision precision) {
        ZonedDateTime fromDate = (nonNull(dateString) && !dateString.isBlank() ? ZonedDateTime.parse(dateString) : ZonedDateTime.now()).truncatedTo(ChronoUnit.SECONDS);
        long epoch = EpochPrecision.millisecond.equals(precision) ? fromDate.toInstant().toEpochMilli() : fromDate.toEpochSecond();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;

        if (copyToClipboard) {
            clipboardService.set(String.valueOf(epoch));
        }

        return asList(
                CommandHelper.bold(String.format("The %ss between epoch and %s is", precision, dateTimeFormatter.format(fromDate))),
                CommandHelper.standardPrefixed("Result", String.valueOf(epoch), true)
        );
    }

    @ShellMethod(value = "Parse epoch timestamp and display as a human readable date", key = {"date parse"})
    public List<AttributedString> fromEpoch(@ShellOption(value = {"--timestamp"}, help = "Timestamp to be converted", defaultValue = CLIPBOARD_FALLBACK) String epochTimestamp,
                                            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result list to clipboard", defaultValue = "false") boolean copyToClipboard) {
        EpochPrecision epochPrecision = epochTimestamp.length() > 10 ? EpochPrecision.millisecond : EpochPrecision.second;
        Function<Long, Instant> conversionFunction = EpochPrecision.millisecond.equals(epochPrecision) ? Instant::ofEpochMilli : Instant::ofEpochSecond;
        long asLong = Long.parseLong(epochTimestamp);
        ZonedDateTime fromDate = conversionFunction.andThen(instant -> instant.atZone(ZoneId.systemDefault())).apply(asLong);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;

        if (copyToClipboard) {
            clipboardService.set(dateTimeFormatter.format(fromDate));
        }

        return singletonList(
                CommandHelper.standardPrefixed("Result", dateTimeFormatter.format(fromDate), true)
        );
    }

    private enum EpochPrecision {
        millisecond,
        second
    }
}
