package uk.co.jamesmcnee.toolbox.comands;

import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.service.ClipboardService;

import java.util.Base64;
import java.util.List;

import static java.util.Collections.singletonList;
import static uk.co.jamesmcnee.toolbox.configuration.ClipboardFallbackParameterResolver.CLIPBOARD_FALLBACK;

@ShellComponent
public class EncodeCommands {

    private final ClipboardService clipboardService;

    @Autowired
    public EncodeCommands(ClipboardService clipboardService) {
        this.clipboardService = clipboardService;
    }

    @ShellMethod(value = "Encode string using base64", key = {"encode", "encode base64"})
    public List<AttributedString> encode64(
            @ShellOption(value = {"--unencoded-string"}, help = "String to encoded", defaultValue = CLIPBOARD_FALLBACK) String unencodedString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the encoded value to clipboard", defaultValue = "false") boolean copyToClipboard) {
        String encoded = Base64.getEncoder().encodeToString(unencodedString.getBytes());

        if (copyToClipboard) {
            clipboardService.set(encoded);
        }

        return singletonList(
                CommandHelper.standardPrefixed("Encoded", encoded, true)
        );
    }

    @ShellMethod(value = "Decode base64 string", key = {"decode", "decode base64"})
    public List<AttributedString> decode64(
            @ShellOption(value = {"--encoded-string"}, help = "String to decode; Uses clipboard when non provided.", defaultValue = CLIPBOARD_FALLBACK) String encodedString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the decoded value to clipboard", defaultValue = "false") boolean copyToClipboard) {
        String decoded = new String(Base64.getDecoder().decode(encodedString));

        if (copyToClipboard) {
            clipboardService.set(decoded);
        }

        return singletonList(
                CommandHelper.standardPrefixed("Decoded", decoded, true)
        );
    }
}
