package uk.co.jamesmcnee.toolbox.comands;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import uk.co.jamesmcnee.toolbox.comands.model.HttpMethod;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.comands.util.ProgressBar;
import uk.co.jamesmcnee.toolbox.domain.HttpTimeResult;
import uk.co.jamesmcnee.toolbox.exception.MalformedURLException;
import uk.co.jamesmcnee.toolbox.service.HttpTestingService;
import uk.co.jamesmcnee.toolbox.util.ValidityUtils;

import java.text.DecimalFormat;
import java.time.Duration;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;
import static java.util.Objects.isNull;
import static java.util.stream.Collectors.toList;
import static uk.co.jamesmcnee.toolbox.configuration.ClipboardFallbackParameterResolver.CLIPBOARD_FALLBACK;
import static uk.co.jamesmcnee.toolbox.util.MathUtils.calculatePercentage;
import static uk.co.jamesmcnee.toolbox.util.MathUtils.calculatePercentile;

@ShellComponent
public class HttpTestingCommands {

    private static final DecimalFormat decimalFormat = new DecimalFormat("###.##");

    private final ObjectMapper objectMapper;
    private final HttpTestingService httpTestingService;
    private final ProgressBar progressBar;

    @Autowired
    public HttpTestingCommands(ObjectMapper objectMapper, HttpTestingService httpTestingService, ProgressBar progressBar) {
        this.objectMapper = objectMapper;
        this.httpTestingService = httpTestingService;
        this.progressBar = progressBar;
    }

    @ShellMethod(value = "Get timing stats from HTTP calls", key = {"http time"})
    public List<AttributedString> timeHttpCall(
            @ShellOption(value = {"-u", "--url"}, help = "URL to test", defaultValue = CLIPBOARD_FALLBACK) String url,
            @ShellOption(value = {"-m", "--method"}, help = "HTTP Method (GET, POST, PUT, DELETE)", defaultValue = "GET") String requestMethod,
            @ShellOption(value = {"-h", "--header"}, help = "Headers to attach to the request. Format: \"{'Key': 'Value', 'Key': 'Value'}\"", defaultValue = "") String headers,
            @ShellOption(value = {"-n", "--number-of-requests"}, help = "The number of requests to make", defaultValue = "1") int numberOfRequests,
            @ShellOption(value = {"--concurrency"}, help = "The number of requests to make at once", defaultValue = "5") int concurrency) {
        if (isNull(url) || !ValidityUtils.urlIsValid(url)) throw new MalformedURLException("URL was null or malformed...");
        HttpMethod httpMethod = HttpMethod.fromString(requestMethod);
        Map<String, String> headerMap = readHeaders(headers);

        List<HttpTimeResult> httpTimeResults = httpTestingService.timeHttpCall(url, httpMethod.toDomain(), headerMap, numberOfRequests, concurrency, progressBar);

        double averageRequestTime = httpTimeResults.stream().map(HttpTimeResult::getDuration).map(Duration::toMillis).mapToLong(Long::valueOf).average().orElse(-1);

        List<Long> millis = httpTimeResults.stream().map(HttpTimeResult::getDuration).map(Duration::toMillis).sorted().collect(toList());
        double fiftiethPercentile = calculatePercentile(millis, 50);
        double ninetyFifthPercentile = calculatePercentile(millis, 95);
        double ninetyNinthPercentile = calculatePercentile(millis, 99);

        long numberOf2xxResponses = getResponseCodesMatching(httpTimeResults, HttpStatus::is2xxSuccessful);
        long numberOf2xxResponsesPct = calculatePercentage(numberOf2xxResponses, httpTimeResults.size());
        long numberOf4xxResponses = getResponseCodesMatching(httpTimeResults, HttpStatus::is4xxClientError);
        long numberOf4xxResponsesPct = calculatePercentage(numberOf4xxResponses, httpTimeResults.size());
        long numberOf5xxResponses = getResponseCodesMatching(httpTimeResults, HttpStatus::is5xxServerError);
        long numberOf5xxResponsesPct = calculatePercentage(numberOf5xxResponses, httpTimeResults.size());
        long numberOfNullResponses = getNullResponseCodes(httpTimeResults);
        long numberOfNullResponsesPct = calculatePercentage(numberOfNullResponses, httpTimeResults.size());

        return asList(
                CommandHelper.standard(String.format("%d requests sent to %s", numberOfRequests, url)),
                CommandHelper.standard("--------------------*--------------------"),
                CommandHelper.blueBold("                 Results                 "),
                CommandHelper.standard("--------------------*--------------------"),
                CommandHelper.standard(String.format("The average request time was: %s millis (mean)", decimalFormat.format(averageRequestTime))),
                CommandHelper.standard(String.format("The 50th percentile was: %s millis", decimalFormat.format(fiftiethPercentile))),
                CommandHelper.standard(String.format("The 95th percentile was: %s millis", decimalFormat.format(ninetyFifthPercentile))),
                CommandHelper.standard(String.format("The 99th percentile was: %s millis", decimalFormat.format(ninetyNinthPercentile))),
                CommandHelper.standard(String.format("The fastest request took: %d millis", millis.get(0))),
                CommandHelper.standard(String.format("The longest request took: %d millis", millis.get(millis.size() - 1))),
                CommandHelper.standard(String.format("Response codes: 2xx: %d (%d%%)  -  4xx: %d (%d%%)  -  5xx: %d (%d%%)  -  Caused exception: %d (%d%%)",
                        numberOf2xxResponses, numberOf2xxResponsesPct, numberOf4xxResponses, numberOf4xxResponsesPct, numberOf5xxResponses,
                        numberOf5xxResponsesPct, numberOfNullResponses, numberOfNullResponsesPct))
        );
    }

    private Map<String, String> readHeaders(String headers) {
        return Optional.ofNullable(headers)
                .filter(string -> !string.isBlank())
                .map(string -> {
                    try {
                        return objectMapper.readValue(string, new TypeReference<Map<String, String>>() {});
                    } catch (JsonProcessingException ignored) {
                        return null;
                    }
                })
                .orElse(emptyMap());
    }

    private long getNullResponseCodes(List<HttpTimeResult> timeResults) {
        return timeResults.stream().map(HttpTimeResult::getResponseStatus).filter(Objects::isNull).count();
    }

    private long getResponseCodesMatching(List<HttpTimeResult> timeResults, Function<HttpStatus, Boolean> checkingFunction) {
        return timeResults.stream().map(HttpTimeResult::getResponseStatus).filter(Objects::nonNull).filter(checkingFunction::apply).count();
    }
}
