package uk.co.jamesmcnee.toolbox.comands;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.service.ClipboardService;

import java.util.List;

import static java.util.Collections.singletonList;
import static uk.co.jamesmcnee.toolbox.configuration.ClipboardFallbackParameterResolver.CLIPBOARD_FALLBACK;

@ShellComponent
public class JsonCommands {

    private final ClipboardService clipboardService;
    private final ObjectMapper objectMapper;

    @Autowired
    public JsonCommands(ClipboardService clipboardService, ObjectMapper objectMapper) {
        this.clipboardService = clipboardService;
        this.objectMapper = objectMapper;
    }

    @ShellMethod(value = "Pretty print JSON", key = {"json prettify"})
    public List<AttributedString> prettyPrintJson(
            @ShellOption(value = {"--json-string"}, help = "JSON String", defaultValue = CLIPBOARD_FALLBACK) String jsonString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result list to clipboard", defaultValue = "false") boolean copyToClipboard) throws JsonProcessingException {
        Object parsedJson;
        try {
            parsedJson = objectMapper.readValue(jsonString, Object.class);
        } catch (Exception e) {
            return singletonList(CommandHelper.error("Input string was not valid JSON or could not be parsed..."));
        }

        String prettyPrinted = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(parsedJson);

        if (copyToClipboard) {
            clipboardService.set(prettyPrinted);
            return singletonList(CommandHelper.success("Prettified JSON has been copied to the clipboard."));
        }

        return singletonList(CommandHelper.standard(prettyPrinted));
    }
}
