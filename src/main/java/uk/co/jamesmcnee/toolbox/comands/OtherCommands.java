package uk.co.jamesmcnee.toolbox.comands;

import org.jline.reader.LineReader;
import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.domain.VersionUpdate;
import uk.co.jamesmcnee.toolbox.service.VersionService;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

@ShellComponent
public class OtherCommands {

    private final LineReader lineReader;
    private final VersionService versionService;

    @Autowired
    public OtherCommands(@Lazy LineReader lineReader, VersionService versionService) {
        this.lineReader = lineReader;
        this.versionService = versionService;
    }

    @ShellMethod(value = "Clear all command history")
    public AttributedString clearHistory() throws IOException {
        lineReader.getHistory().purge();

        return CommandHelper.standardPrefixed("Info", "Command history has now been purged!");
    }

    @ShellMethod(value = "Get the current version", key = {"version"})
    public AttributedString getCurrentVersion() {
        return CommandHelper.standardPrefixed("Version", versionService.getCurrentVersion(), true);
    }

    @ShellMethod(value = "Check for updates", key = {"update"})
    public List<AttributedString> checkForUpdates() {
        Optional<VersionUpdate> versionUpdate = versionService.getVersionUpdate();

        return versionUpdate.map(update -> asList(
                CommandHelper.blueBold("There is a new version available!"),
                CommandHelper.standardPrefixed("Current version", update.getCurrentVersion(), true),
                CommandHelper.standardPrefixed("New version", update.getLatestVersion().getNewVersion(), true),
                CommandHelper.standardPrefixed("Download from", update.getLatestVersion().getNewVersionUrl(), true)
        )).orElse(singletonList(CommandHelper.blueBold("There are no updates available!")));

    }
}
