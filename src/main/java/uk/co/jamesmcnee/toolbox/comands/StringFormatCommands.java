package uk.co.jamesmcnee.toolbox.comands;

import org.jline.utils.AttributedString;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;
import uk.co.jamesmcnee.toolbox.comands.util.CommandHelper;
import uk.co.jamesmcnee.toolbox.service.ClipboardService;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static uk.co.jamesmcnee.toolbox.configuration.ClipboardFallbackParameterResolver.CLIPBOARD_FALLBACK;

@ShellComponent
public class StringFormatCommands {

    private final ClipboardService clipboardService;

    @Autowired
    public StringFormatCommands(ClipboardService clipboardService) {
        this.clipboardService = clipboardService;
    }

    @ShellMethod(value = "Print CSV as List", key = {"string-format listify"})
    public List<AttributedString> csvToList(
            @ShellOption(value = {"--csv-string"}, help = "CSV String", defaultValue = CLIPBOARD_FALLBACK) String csvString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result list to clipboard", defaultValue = "false") boolean copyToClipboard) {
        List<String> asList = Arrays.stream(csvString.split(",")).map(String::trim).collect(toList());

        if (copyToClipboard) {
            clipboardService.set(String.join("\n", asList));
        }

        return Stream.of(
                Stream.of(CommandHelper.standard("-----*-----")),
                Stream.of(CommandHelper.blueBold("Result List")),
                Stream.of(CommandHelper.standard("-----*-----")),
                asList.stream().map(CommandHelper::standard),
                Stream.of(CommandHelper.standard("-----*-----"))
        ).flatMap(Stream::distinct).collect(toList());
    }

    @ShellMethod(value = "Print List as CSV", key = {"string-format csv"})
    public List<AttributedString> listToCSV(
            @ShellOption(value = {"--list-string"}, help = "Input String", defaultValue = CLIPBOARD_FALLBACK) String listString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result CSV to clipboard", defaultValue = "false") boolean copyToClipboard,
            @ShellOption(value = {"--disable-space-between"}, help = "Space between elements", defaultValue = "true") boolean spaceBetweenElements) {
        String csvString = String.join(spaceBetweenElements ? ", " : ",", listString.split("\\r?\\n"));

        if (copyToClipboard) {
            clipboardService.set(csvString);
        }

        return singletonList(
                CommandHelper.standardPrefixed("Result", csvString, true)
        );
    }

    @ShellMethod(value = "Strip all spaces from string", key = {"string-format strip-spaces"})
    public List<AttributedString> stripSpaces(
            @ShellOption(value = {"--input-string"}, help = "Input String", defaultValue = CLIPBOARD_FALLBACK) String inputString,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result CSV to clipboard", defaultValue = "false") boolean copyToClipboard) {
        String strippedString = inputString.replaceAll(" ", "");

        if (copyToClipboard) {
            clipboardService.set(strippedString);
        }

        return singletonList(
                CommandHelper.standardPrefixed("Result", strippedString, true)
        );
    }

    @ShellMethod(value = "Replace all instances of a string with another", key = {"string-format replace"})
    public List<AttributedString> replaceAl(
            @ShellOption(value = {"--input-string"}, help = "Input String", defaultValue = CLIPBOARD_FALLBACK) String inputString,
            @ShellOption(value = {"-f", "--find-string"}, help = "Find String") String find,
            @ShellOption(value = {"-r", "--replacement-string"}, help = "Replacement String") String replace,
            @ShellOption(value = {"-c", "--copy"}, help = "Copy the result CSV to clipboard", defaultValue = "false") boolean copyToClipboard) {
        String replacedString = inputString.replaceAll(find, replace);

        if (copyToClipboard) {
            clipboardService.set(replacedString);
        }

        return singletonList(
                CommandHelper.standardPrefixed("Result", replacedString, true)
        );
    }


}
