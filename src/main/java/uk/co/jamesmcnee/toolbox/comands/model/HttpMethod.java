package uk.co.jamesmcnee.toolbox.comands.model;

import uk.co.jamesmcnee.toolbox.exception.UnacceptableValueProvidedException;

import java.util.stream.Stream;

public enum HttpMethod {
    GET,
    PUT,
    POST,
    PATCH,
    DELETE;

    public org.springframework.http.HttpMethod toDomain() {
        return org.springframework.http.HttpMethod.resolve(this.name());
    }

    public static HttpMethod fromString(String stringValue) {
        return Stream.of(HttpMethod.values())
                .filter(method -> method.name().equalsIgnoreCase(stringValue))
                .findFirst()
                .orElseThrow(() -> new UnacceptableValueProvidedException(String.format("%s is not an acceptable HttpMethod!", stringValue)));
    }
}
