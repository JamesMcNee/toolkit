package uk.co.jamesmcnee.toolbox.comands.util;

import org.jline.terminal.Terminal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;
import uk.co.jamesmcnee.toolbox.domain.ProgressMonitor;
import uk.co.jamesmcnee.toolbox.exception.AlreadyBeingUsedException;
import uk.co.jamesmcnee.toolbox.exception.SessionNotStatedException;

@Component
public class ProgressBar implements ProgressMonitor {

    private static final String CUU = "\u001B[A";
    private static final String DL = "\u001B[1M";

    private String doneMarker = "■";
    private String remainsMarker = "□";
    private String leftDelimiter = "<";
    private String rightDelimiter = ">";

    private boolean started = false;

    private int currentStep;
    private int totalNumber;
    private int stepSize;

    Terminal terminal;

    @Autowired
    public ProgressBar(@Lazy Terminal terminal) {
        this.terminal = terminal;
    }

    public synchronized void startSession(int totalNumber) {
        if (!started) {
            this.started = true;
            this.terminal.writer().println();
            this.totalNumber = totalNumber;
            this.currentStep = 0;
            this.stepSize = totalNumber / 100;
        } else {
            throw new AlreadyBeingUsedException("Session has already been started. Please stop the session first.");
        }
    }

    public synchronized void increment() {
        this.currentStep++;
        int percentage = (int) Math.ceil(((double) currentStep / totalNumber) * 100);

        reportProgress(percentage);
        if (percentage >= 100) stopSession();
    }

    private void reportProgress(int percentage) {
        if (!started) {
            throw new SessionNotStatedException("Session needs to be started first");
        }

        int x = (percentage/5);
        int y = 20-x;

        String done = new String(new char[x]).replace("\0", doneMarker);
        String remains = new String(new char[y]).replace("\0", remainsMarker);

        String progressBar = String.format("%s%s%s%s %d%% (%d/%d)", leftDelimiter, done, remains, rightDelimiter, percentage, currentStep, totalNumber);

        terminal.writer().println(CUU + "\r" + DL + progressBar);
        terminal.flush();
    }

    public synchronized void stopSession() {
        this.started = false;
    }
}
