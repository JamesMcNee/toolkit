package uk.co.jamesmcnee.toolbox.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties("build")
public class BuildConfiguration {

    public static final String DEVELOPMENT_VERSION = "DEVELOPMENT";

    private String version;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
