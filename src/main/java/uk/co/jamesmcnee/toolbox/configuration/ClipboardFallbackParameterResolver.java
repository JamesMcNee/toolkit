package uk.co.jamesmcnee.toolbox.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.ConversionService;
import org.springframework.shell.ValueResult;
import org.springframework.shell.standard.ShellOption;
import org.springframework.shell.standard.StandardParameterResolver;
import org.springframework.stereotype.Component;
import uk.co.jamesmcnee.toolbox.exception.NoValueProvidedException;
import uk.co.jamesmcnee.toolbox.service.ClipboardService;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class ClipboardFallbackParameterResolver extends StandardParameterResolver {

    public static final String CLIPBOARD_FALLBACK = "CLIPBOARD_FALLBACK";

    private final ClipboardService clipboardService;

    @Autowired
    public ClipboardFallbackParameterResolver(ConversionService conversionService, ClipboardService clipboardService) {
        super(conversionService);
        this.clipboardService = clipboardService;
    }

    @Override
    public ValueResult resolve(MethodParameter methodParameter, List<String> wordsBuffer) {
        if (methodParameter.getParameterIndex() == 0) {
            List<? extends ShellOption> possibleCommands = Stream.of(methodParameter.getMethod()).filter(Objects::nonNull)
                    .map(Method::getParameterAnnotations).flatMap(Stream::of).flatMap(Stream::of)
                    .filter(annotation -> annotation.annotationType().equals(ShellOption.class)).map(x -> (ShellOption) x).collect(toList());

            ShellOption firstOption = possibleCommands.get(0);
            if (firstOption.defaultValue().equals(CLIPBOARD_FALLBACK)) {
                List<String> possibleCommandValues = possibleCommands.stream().map(ShellOption::value).flatMap(Stream::of).collect(toList());
                if (wordsBuffer.isEmpty() || (possibleCommandValues.stream().anyMatch(commandValue -> commandValue.equals(wordsBuffer.get(0))) &&
                        Stream.of(firstOption).map(ShellOption::value).flatMap(Arrays::stream).noneMatch(wordsBuffer::contains))) {

                    String clipboardValue = clipboardService.get()
                            .orElseThrow(() -> new NoValueProvidedException("No value provided and nothing available in clipboard..."));

                    return new ValueResult(methodParameter, clipboardValue);
                }
            }
        }

        return super.resolve(methodParameter, wordsBuffer);
    }
}
