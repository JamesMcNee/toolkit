package uk.co.jamesmcnee.toolbox.configuration;

import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStyle;
import org.springframework.context.annotation.Configuration;
import org.springframework.shell.jline.PromptProvider;

@Configuration
public class PromptConfiguration implements PromptProvider {

    @Override
    public AttributedString getPrompt() {
        return new AttributedString("Toolbox: ", AttributedStyle.BOLD.foreground(AttributedStyle.MAGENTA));
    }

}
