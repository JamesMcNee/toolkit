package uk.co.jamesmcnee.toolbox.domain;

import org.springframework.http.HttpStatus;

import java.time.Duration;

public class HttpTimeResult {

    private final Duration duration;
    private final HttpStatus responseStatus;

    private HttpTimeResult(Builder builder) {
        this.duration = builder.duration;
        this.responseStatus = builder.responseStatus;
    }

    public Duration getDuration() {
        return duration;
    }

    public HttpStatus getResponseStatus() {
        return responseStatus;
    }


    public static final class Builder {

        private Duration duration;
        private HttpStatus responseStatus;

        public Builder withDuration(Duration duration) {
            this.duration = duration;
            return this;
        }

        public Builder withResponseStatus(HttpStatus responseStatus) {
            this.responseStatus = responseStatus;
            return this;
        }

        public HttpTimeResult build() {
            return new HttpTimeResult(this);
        }
    }
}
