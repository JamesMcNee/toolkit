package uk.co.jamesmcnee.toolbox.domain;

public class LatestVersion {

    private final String newVersion;
    private final String newVersionUrl;

    private LatestVersion(Builder builder) {
        this.newVersion = builder.newVersion;
        this.newVersionUrl = builder.newVersionUrl;
    }

    public String getNewVersion() {
        return newVersion;
    }

    public String getNewVersionUrl() {
        return newVersionUrl;
    }


    public static final class Builder {

        private String newVersion;
        private String newVersionUrl;

        public Builder withNewVersion(String newVersion) {
            this.newVersion = newVersion;
            return this;
        }

        public Builder withNewVersionUrl(String newVersionUrl) {
            this.newVersionUrl = newVersionUrl;
            return this;
        }

        public LatestVersion build() {
            return new LatestVersion(this);
        }
    }
}
