package uk.co.jamesmcnee.toolbox.domain;

public interface ProgressMonitor {

    void startSession(int totalNumber);

    void increment();

    void stopSession();
}
