package uk.co.jamesmcnee.toolbox.domain;

public class VersionUpdate {

    private final String currentVersion;
    private final LatestVersion latestVersion;

    private VersionUpdate(Builder builder) {
        currentVersion = builder.currentVersion;
        latestVersion = builder.latestVersion;
    }

    public String getCurrentVersion() {
        return currentVersion;
    }

    public LatestVersion getLatestVersion() {
        return latestVersion;
    }


    public static final class Builder {

        private String currentVersion;
        private LatestVersion latestVersion;

        public Builder withCurrentVersion(String currentVersion) {
            this.currentVersion = currentVersion;
            return this;
        }

        public Builder withLatestVersion(LatestVersion latestVersion) {
            this.latestVersion = latestVersion;
            return this;
        }

        public VersionUpdate build() {
            return new VersionUpdate(this);
        }
    }
}
