package uk.co.jamesmcnee.toolbox.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class LatestVersionEntity {

    private final String value;
    private final String url;

    @JsonCreator
    public LatestVersionEntity(@JsonProperty("value") String value, @JsonProperty("url") String url) {
        this.value = value;
        this.url = url;
    }

    public String getValue() {
        return value;
    }

    public String getUrl() {
        return url;
    }
}
