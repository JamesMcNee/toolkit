package uk.co.jamesmcnee.toolbox.entity;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import uk.co.jamesmcnee.toolbox.domain.LatestVersion;

@JsonIgnoreProperties(ignoreUnknown = true)
public class VersionUpdateEntity {

    private final LatestVersionEntity latestVersion;

    @JsonCreator
    public VersionUpdateEntity(@JsonProperty("latestVersion") LatestVersionEntity latestVersion) {
        this.latestVersion = latestVersion;
    }

    public LatestVersionEntity getLatestVersion() {
        return latestVersion;
    }

    public LatestVersion toDomain() {
        return new LatestVersion.Builder()
                .withNewVersion(latestVersion.getValue())
                .withNewVersionUrl(latestVersion.getUrl())
                .build();
    }
}
