package uk.co.jamesmcnee.toolbox.exception;

public class AlreadyBeingUsedException extends RuntimeException {
    public AlreadyBeingUsedException(String message) {
        super(message);
    }
}
