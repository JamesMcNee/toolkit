package uk.co.jamesmcnee.toolbox.exception;

public class MalformedURLException extends RuntimeException {
    public MalformedURLException(String message) {
        super(message);
    }
}
