package uk.co.jamesmcnee.toolbox.exception;

public class NoValueProvidedException extends RuntimeException {
    public NoValueProvidedException(String message) {
        super(message);
    }
}
