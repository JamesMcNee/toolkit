package uk.co.jamesmcnee.toolbox.exception;

public class SessionNotStatedException extends RuntimeException {
    public SessionNotStatedException(String message) {
        super(message);
    }
}
