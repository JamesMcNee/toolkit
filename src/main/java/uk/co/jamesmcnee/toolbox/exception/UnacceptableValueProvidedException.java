package uk.co.jamesmcnee.toolbox.exception;

public class UnacceptableValueProvidedException extends RuntimeException {
    public UnacceptableValueProvidedException(String message) {
        super(message);
    }
}
