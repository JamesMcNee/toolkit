package uk.co.jamesmcnee.toolbox.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.util.StopWatch;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import uk.co.jamesmcnee.toolbox.domain.HttpTimeResult;

import java.time.Duration;
import java.util.Map;

import static java.util.Objects.nonNull;

@Repository
public class HttpTimeRepository {

    private final RestTemplate restTemplate;

    @Autowired
    public HttpTimeRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public HttpTimeResult timeHttpCall(String url, HttpMethod httpMethod, Map<String, String> headers) {
        StopWatch stopWatch = new StopWatch();
        stopWatch.start();
        ResponseEntity<Void> response;
        try {
             response = restTemplate.exchange(url, httpMethod, new HttpEntity<>(createHeaders(headers)), Void.class);
        } catch (Exception e) {
            if (e instanceof HttpClientErrorException) {
                response = new ResponseEntity<>(((HttpClientErrorException) e).getStatusCode());
            } else {
                response = null;
            }
        }
        stopWatch.stop();

        return new HttpTimeResult.Builder()
                .withDuration(Duration.ofNanos(stopWatch.getTotalTimeNanos()))
                .withResponseStatus(nonNull(response) ? response.getStatusCode() : null)
                .build();
    }

    private HttpHeaders createHeaders(Map<String, String> headers) {
        HttpHeaders httpHeaders = new HttpHeaders();
        headers.forEach(httpHeaders::add);
        return httpHeaders;
    }
}
