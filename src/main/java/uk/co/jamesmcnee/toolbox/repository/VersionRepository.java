package uk.co.jamesmcnee.toolbox.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import uk.co.jamesmcnee.toolbox.domain.LatestVersion;
import uk.co.jamesmcnee.toolbox.entity.VersionUpdateEntity;

import java.util.Optional;

@Repository
public class VersionRepository {

    private final RestTemplate restTemplate;

    @Autowired
    public VersionRepository(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public Optional<LatestVersion> getVersionUpdate() {
        try {
            ResponseEntity<VersionUpdateEntity> versionUpdate = restTemplate.exchange("https://download.jamesmcnee.com/toolbox/version.php", HttpMethod.GET,
                    null, VersionUpdateEntity.class);

            return Optional.of(versionUpdate).map(ResponseEntity::getBody).map(VersionUpdateEntity::toDomain);
        } catch (Exception ignored) {
            return Optional.empty();
        }
    }
}
