package uk.co.jamesmcnee.toolbox.service;

import org.springframework.stereotype.Service;

import java.awt.*;
import java.awt.datatransfer.*;
import java.io.IOException;
import java.util.Optional;

@Service
public class ClipboardService {

    private final Clipboard clipboard;

    public ClipboardService() {
        System.setProperty("apple.awt.UIElement", "true");
        System.setProperty("java.awt.headless", "false");
        clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    public void set(String string) {
        StringSelection selection = new StringSelection(string);
        clipboard.setContents(selection, selection);
    }

    public Optional<String> get() {
        try {
            return Optional.ofNullable(clipboard.getData(DataFlavor.stringFlavor).toString()).filter(s -> !s.isBlank());
        } catch (UnsupportedFlavorException | IOException ignored) {}

        return Optional.empty();
    }

}
