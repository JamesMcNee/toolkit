package uk.co.jamesmcnee.toolbox.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import uk.co.jamesmcnee.toolbox.domain.HttpTimeResult;
import uk.co.jamesmcnee.toolbox.domain.ProgressMonitor;
import uk.co.jamesmcnee.toolbox.repository.HttpTimeRepository;

import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.stream.IntStream;

import static java.util.stream.Collectors.toList;

@Service
public class HttpTestingService {

    private final HttpTimeRepository httpTimeRepository;

    @Autowired
    public HttpTestingService(HttpTimeRepository httpTimeRepository) {
        this.httpTimeRepository = httpTimeRepository;
    }

    public List<HttpTimeResult> timeHttpCall(String url, HttpMethod httpMethod, Map<String, String> headers,
                                             int numberOfRequests, int concurrency, ProgressMonitor progressMonitor) {
        ThreadPoolTaskExecutor taskExecutor = createThreadPool(concurrency);

        progressMonitor.startSession(numberOfRequests);

        List<HttpTimeResult> timeResults = IntStream.range(0, numberOfRequests)
                .parallel()
                .mapToObj(t -> CompletableFuture.supplyAsync(() -> httpTimeRepository.timeHttpCall(url, httpMethod, headers), taskExecutor))
                .peek(c -> c.thenAccept(f -> progressMonitor.increment()))
                .map(CompletableFuture::join)
                .collect(toList());

        progressMonitor.stopSession();
        taskExecutor.shutdown();

        return timeResults;
    }

    private ThreadPoolTaskExecutor createThreadPool(int concurrency) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(concurrency);
        executor.setMaxPoolSize(concurrency);
        executor.setThreadNamePrefix("http-time-test");
        executor.initialize();
        return executor;
    }
}
