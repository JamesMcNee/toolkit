package uk.co.jamesmcnee.toolbox.service;

import com.github.zafarkhaja.semver.Version;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uk.co.jamesmcnee.toolbox.configuration.BuildConfiguration;
import uk.co.jamesmcnee.toolbox.domain.LatestVersion;
import uk.co.jamesmcnee.toolbox.domain.VersionUpdate;
import uk.co.jamesmcnee.toolbox.repository.VersionRepository;

import javax.annotation.PostConstruct;
import java.util.Optional;

import static uk.co.jamesmcnee.toolbox.configuration.BuildConfiguration.DEVELOPMENT_VERSION;

@Service
public class VersionService {

    private final BuildConfiguration buildConfiguration;
    private final VersionRepository versionRepository;

    @Autowired
    public VersionService(BuildConfiguration buildConfiguration, VersionRepository versionRepository) {
        this.buildConfiguration = buildConfiguration;
        this.versionRepository = versionRepository;
    }

    public String getCurrentVersion() {
        return buildConfiguration.getVersion();
    }

    public Optional<VersionUpdate> getVersionUpdate() {
        return versionRepository.getVersionUpdate().map(latestVersionObj -> {
            String currentVersionString = getCurrentVersion();
            if (currentVersionString.equals(DEVELOPMENT_VERSION)) return null;

            Version currentVersion = Version.valueOf(currentVersionString);
            Version latestVersion = Version.valueOf(latestVersionObj.getNewVersion());

            if (currentVersion.lessThan(latestVersion)) {
                return new VersionUpdate.Builder()
                        .withCurrentVersion(currentVersionString)
                        .withLatestVersion(new LatestVersion.Builder()
                                .withNewVersion(latestVersionObj.getNewVersion())
                                .withNewVersionUrl(latestVersionObj.getNewVersionUrl())
                                .build())
                        .build();
            }

            return null;
        });
    }

    @PostConstruct
    public void autoCheckForUpdate() {
        getVersionUpdate().ifPresent(versionUpdate -> {
            System.out.println("There is a new version available!");
            System.out.println("Current version: " + versionUpdate.getCurrentVersion());
            System.out.println("New version: " + versionUpdate.getLatestVersion().getNewVersion());
            System.out.println("Download from: " + versionUpdate.getLatestVersion().getNewVersionUrl());

        });
    }
}
