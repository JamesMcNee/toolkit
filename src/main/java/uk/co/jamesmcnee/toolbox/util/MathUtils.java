package uk.co.jamesmcnee.toolbox.util;

import java.util.List;

import static java.util.stream.Collectors.toList;

public abstract class MathUtils {

    public static double calculatePercentile(List<Long> longs, int percentile) {
        if (longs.size() == 1) return longs.get(0);

        List<Long> sortedLongs = longs.stream().sorted(Long::compareTo).collect(toList());

        double index = (percentile / 100.0) * sortedLongs.size();

        // If whole number...
        if ((index % 1) == 0) {
            int roundedIndex = ((int) Math.ceil(index)) - 1;
            Long one = sortedLongs.get(roundedIndex);
            Long two = sortedLongs.get(roundedIndex + 1);

            return (one + two) / 2.0;
        } else {
            int intIndex = ((int) Math.abs(index)) - 1;

            return sortedLongs.get(intIndex);
        }
    }

    public static int calculatePercentage(int number, int percentageOf) {
        return (int) calculatePercentage((long) number, percentageOf);
    }

    public static long calculatePercentage(long number, int percentageOf) {
        return (int) Math.ceil(((double) number / percentageOf) * 100);
    }

}
