package uk.co.jamesmcnee.toolbox.util;

import java.net.URL;

public abstract class ValidityUtils {

    public static boolean urlIsValid(String url) {
        try {
            new URL(url).toURI();

            return true;
        } catch (Exception e) { return false; }
    }
}
